package com.example.qrgames.others;

public class QrCallback {

    public interface Callback {
        void callingBack(String s);
        void callingBackCanRead(boolean s);
    }

    private Callback callback;

    public void registerCallBack(Callback callback) {
        this.callback = callback;
    }

    public void returnQr(String s) {
        callback.callingBack(s);
    }

    public void setCanRead(boolean s) {
        callback.callingBackCanRead(s);
    }
}
