package com.example.qrgames;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.qrgames.others.QrCallback;

public class QuestionsActivity extends AppCompatActivity {
    public int stage;
    public static int qrText;
    public static MainActivity mainActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        stage = 0;

        getSupportActionBar().hide();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setNavigationBarColor(Color.BLACK);

        setContentView(R.layout.activity_questions);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        setStage();
    }

    private void setStage() {
        LinearLayout ll = (LinearLayout) findViewById(R.id.contentLinear);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        ll.removeAllViews();
        switch (qrText) {
            case 1:
                ((ImageView) findViewById(R.id.imageView)).setImageResource(R.drawable.question_1);
                switch (stage) {
                    case 0: {
                        Button myButton;
                        String question = "Какое вооружение относится к артиллерии? ";
                        String[] answers = {"Пушки", "Гаубицы", "Миномёты", "Все варианты"};
                        int right = 4;
                        right--;

                        TextView questionText = new TextView(this);
                        questionText.setText(question);
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                            
                        for (int i = 0; i < answers.length; i++) {
                            if (i == right) {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stage++;
                                        setStage();
                                    }
                                });
                                ll.addView(myButton, lp);
                            } else {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        v.setBackgroundColor(0xFFFFB6C1);
                                    }
                                });
                                ll.addView(myButton, lp);
                            }
                        }

                        ll.requestLayout();
                        break;
                    }
                    case 1: {
                        TextView questionText = new TextView(this);
                        questionText.setText("Верно. Махмут Аипов героически погиб, когда ему было 24 года.");
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        Button myButton = new Button(this);
                        myButton.setText("Далее");
                        myButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                stage++;
                                setStage();
                            }
                        });

                        ll.addView(myButton, lp);
                        ll.requestLayout();
                        break;
                    }
                    case 2: {
                        Button myButton;
                        String question = "В какой период времени 671-ый артиллерийский полк принимал участие в Великой Отечественной войне? ";
                        String[] answers = {"с 1942 по 1943гг.", "с 1941 по 1944 гг.", "с 1943 по 1945гг.", "с 1941 по 1945 гг."};
                        int right = 2;
                        right--;

                        TextView questionText = new TextView(this);
                        questionText.setText(question);
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        for (int i = 0; i < answers.length; i++) {
                            if (i == right) {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stage++;
                                        setStage();
                                    }
                                });
                                ll.addView(myButton, lp);
                            } else {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        v.setBackgroundColor(0xFFFFB6C1);
                                    }
                                });
                                ll.addView(myButton, lp);
                            }
                        }

                        ll.requestLayout();
                        break;
                    }
                    case 3: {
                        TextView questionText = new TextView(this);
                        questionText.setText("Верно. 4 года учились дети в начальной школе 30-х годов в СССР. По данным перепеси, больше половины жителей Советской России в 20-х годах не умела читать и писать. Советское руководство ставило перед собой задачу победить неграмотность советского народа, среди которых было много крестьян. Только к 1930 году удалось ввести начальное образование на всей территории СССР.");
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        Button myButton = new Button(this);
                        myButton.setText("Далее");
                        myButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                stage++;
                                setStage();
                            }
                        });

                        ll.addView(myButton, lp);
                        ll.requestLayout();
                        break;
                    }
                    case 4: {
                        // добавить правильный овтет
                        Button myButton;
                        String question = "Сколько солдат входит в состав одного полка?";
                        String[] answers = {"от 1000 до 2000", "от 3000 до 4000", "от 4000 до 5000", "от 2000 до 3000"};
                        int right = 2; // изменить
                        right--;

                        TextView questionText = new TextView(this);
                        questionText.setText(question);
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        for (int i = 0; i < answers.length; i++) {
                            if (i == right) {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stage++;
                                        setStage();
                                    }
                                });
                                ll.addView(myButton, lp);
                            } else {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        v.setBackgroundColor(0xFFFFB6C1);
                                    }
                                });
                                ll.addView(myButton, lp);
                            }
                        }

                        ll.requestLayout();
                        break;
                    }
                    case 5: {
                        TextView questionText = new TextView(this);
                        questionText.setText("Верно. Дорога на телеге с лошадью от Кирюшкино до Костычей занимала 9,5 часов. В наше время, это же расстояние можно преодолеть на машине за 1 час 40 минут, двигаясь со скоростью 90 км/ч.");
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        Button myButton = new Button(this);
                        myButton.setText("Далее");
                        myButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SharedPreferences sharedPref = mainActivity.getPreferences(Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.putBoolean("1", true);
                                editor.commit();
                                mainActivity.canRead = true;
                                mainActivity.refreshPhoto();
                                finish();
                            }
                        });

                        ll.addView(myButton, lp);
                        ll.requestLayout();
                        break;
                    }
                }
                break;
            case 2:
                ((ImageView) findViewById(R.id.imageView)).setImageResource(R.drawable.question_2);
                switch (stage) {

                    case 0: {
                        Button myButton;
                        String question = "Что такое стратегия?";
                        String[] answers = {"Наука о ведении войны, одна из областей военного искусства, высшее его проявление", "Составная часть военного искусства, включающая теорию и практику подготовки и ведения боя", "Все варианты"};
                        int right = 1;
                        right--;

                        TextView questionText = new TextView(this);
                        questionText.setText(question);
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        for (int i = 0; i < answers.length; i++) {
                            if (i == right) {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stage++;
                                        setStage();
                                    }
                                });
                                ll.addView(myButton, lp);
                            } else {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        v.setBackgroundColor(0xFFFFB6C1);
                                    }
                                });
                                ll.addView(myButton, lp);
                            }
                        }

                        ll.requestLayout();
                        break;
                    }
                    case 1: {
                        TextView questionText = new TextView(this);
                        questionText.setText("Верно. Махмут был ранен через полгода с момента, как попал на фронт.");
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        Button myButton = new Button(this);
                        myButton.setText("Далее");
                        myButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                stage++;
                                setStage();
                            }
                        });

                        ll.addView(myButton, lp);
                        ll.requestLayout();
                        break;
                    }
                    case 2: {
                        Button myButton;
                        String question = "Какое условное обозначение получила операция по захвату фашистскими войсками Курска и близлежащих городов?";
                        String[] answers = {"«Цитадель»", "«Север»", "«Барбаросса»", "«Юг»"};
                        int right = 1;
                        right--;

                        TextView questionText = new TextView(this);
                        questionText.setText(question);
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        for (int i = 0; i < answers.length; i++) {
                            if (i == right) {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stage++;
                                        setStage();
                                    }
                                });
                                ll.addView(myButton, lp);
                            } else {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        v.setBackgroundColor(0xFFFFB6C1);
                                    }
                                });
                                ll.addView(myButton, lp);
                            }
                        }

                        ll.requestLayout();
                        break;
                    }
                    case 3: {
                        TextView questionText = new TextView(this);
                        questionText.setText("Верно. Михаил был старшим братом Махмута. Он первым отправился на фронт и погиб в 1942 году.");
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        Button myButton = new Button(this);
                        myButton.setText("Далее");
                        myButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                stage++;
                                setStage();
                            }
                        });

                        ll.addView(myButton, lp);
                        ll.requestLayout();
                        break;
                    }
                    case 4: {
                        Button myButton;
                        String question = "Сколько месяцев длилась Курская битва?";
                        String[] answers = {"1,5 месяца", "2 месяца", "2,5 месяца", "1 месяц"};
                        int right = 1;
                        right--;

                        TextView questionText = new TextView(this);
                        questionText.setText(question);
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        for (int i = 0; i < answers.length; i++) {
                            if (i == right) {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stage++;
                                        setStage();
                                    }
                                });
                                ll.addView(myButton, lp);
                            } else {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        v.setBackgroundColor(0xFFFFB6C1);
                                    }
                                });
                                ll.addView(myButton, lp);
                            }
                        }

                        ll.requestLayout();
                        break;
                    }
                    case 5: {
                        TextView questionText = new TextView(this);
                        questionText.setText("Верно. Михаил был старшим братом Махмута. Он первым отправился на фронт и погиб в 1942 году.");
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        Button myButton = new Button(this);
                        myButton.setText("Далее");
                        myButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SharedPreferences sharedPref = mainActivity.getPreferences(Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.putBoolean("2", true);
                                editor.commit();
                                mainActivity.canRead = true;
                                mainActivity.refreshPhoto();
                                finish();
                            }
                        });

                        ll.addView(myButton, lp);
                        ll.requestLayout();
                        break;
                    }
                }
                break;
            case 3:
                ((ImageView) findViewById(R.id.imageView)).setImageResource(R.drawable.question_3);
                switch (stage) {
                    case 0: {
                        Button myButton;
                        String question = "Чем отличается рядовой солдат от офицера?";
                        String[] answers = {"Формой одежды", "Воинскими знаками различия", "Функциями выполнения боевых задач", "Все варианты"};
                        int right = 4;
                        right--;

                        TextView questionText = new TextView(this);
                        questionText.setText(question);
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        for (int i = 0; i < answers.length; i++) {
                            if (i == right) {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stage++;
                                        setStage();
                                    }
                                });
                                ll.addView(myButton, lp);
                            } else {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        v.setBackgroundColor(0xFFFFB6C1);
                                    }
                                });
                                ll.addView(myButton, lp);
                            }
                        }

                        ll.requestLayout();
                        break;
                    }
                    case 1: {
                        TextView questionText = new TextView(this);
                        questionText.setText("Верно. Медаль за отвагу была учреждена 17 октября 1938 года. 19 октября 1938 года прошло первое награждение. Среди первых награждённых этой медалью были пограничники Н. Гуляев и Ф. Григорьев, задержавшие группу диверсантов у озера Хасан. «За отвагу» — высшая медаль в наградной системе СССР.");
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        Button myButton = new Button(this);
                        myButton.setText("Далее");
                        myButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                stage++;
                                setStage();
                            }
                        });

                        ll.addView(myButton, lp);
                        ll.requestLayout();
                        break;
                    }
                    case 2: {
                        Button myButton;
                        String question = "Что такое дивизия?";
                        String[] answers = {"Соединение военных кораблей одного или нескольких классов ", "Войсковое соединение в составе нескольких полков", "Основное тактическое соединение в различных видах вооруженных сил", "Все варианты"};
                        int right = 4;
                        right--;

                        TextView questionText = new TextView(this);
                        questionText.setText(question);
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        for (int i = 0; i < answers.length; i++) {
                            if (i == right) {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stage++;
                                        setStage();
                                    }
                                });
                                ll.addView(myButton, lp);
                            } else {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        v.setBackgroundColor(0xFFFFB6C1);
                                    }
                                });
                                ll.addView(myButton, lp);
                            }
                        }

                        ll.requestLayout();
                        break;
                    }
                    case 3: {
                        TextView questionText = new TextView(this);
                        questionText.setText("Верно. Махмут получил две медали за отвагу. Обе в 1944 году.");
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        Button myButton = new Button(this);
                        myButton.setText("Далее");
                        myButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                stage++;
                                setStage();
                            }
                        });

                        ll.addView(myButton, lp);
                        ll.requestLayout();
                        break;
                    }
                    case 4: {
                        Button myButton;
                        String question = "Сколько звезд на погоне капитана?";
                        String[] answers = {"2", "4", "3", "1"};
                        int right = 2;
                        right--;

                        TextView questionText = new TextView(this);
                        questionText.setText(question);
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        for (int i = 0; i < answers.length; i++) {
                            if (i == right) {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stage++;
                                        setStage();
                                    }
                                });
                                ll.addView(myButton, lp);
                            } else {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        v.setBackgroundColor(0xFFFFB6C1);
                                    }
                                });
                                ll.addView(myButton, lp);
                            }
                        }

                        ll.requestLayout();
                        break;
                    }
                    case 5: {
                        TextView questionText = new TextView(this);
                        questionText.setText("Верно. В слове \"учавствовал\" лишняя буква \"в\". С этой буквы начинается слово \"Война\" - лишнее слово в истории человечества, так хочется верить..");
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        Button myButton = new Button(this);
                        myButton.setText("Далее");
                        myButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SharedPreferences sharedPref = mainActivity.getPreferences(Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.putBoolean("3", true);
                                editor.commit();
                                mainActivity.canRead = true;
                                mainActivity.refreshPhoto();
                                finish();
                            }
                        });

                        ll.addView(myButton, lp);
                        ll.requestLayout();
                        break;
                    }
                }
                break;
            case 4:
                ((ImageView) findViewById(R.id.imageView)).setImageResource(R.drawable.question_4);
                switch (stage) {
                    case 0: {
                        Button myButton;
                        String question = "С какого по какой месяц длилась Битва за Днепр?";
                        String[] answers = {"С июля по сентябрь 1943", "С августа по октябрь 1943", "С сентября по декабрь 1943", "С августа по декабрь 1943"};
                        int right = 4;
                        right--;

                        TextView questionText = new TextView(this);
                        questionText.setText(question);
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        for (int i = 0; i < answers.length; i++) {
                            if (i == right) {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stage++;
                                        setStage();
                                    }
                                });
                                ll.addView(myButton, lp);
                            } else {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        v.setBackgroundColor(0xFFFFB6C1);
                                    }
                                });
                                ll.addView(myButton, lp);
                            }
                        }

                        ll.requestLayout();
                        break;
                    }
                    case 1: {
                        TextView questionText = new TextView(this);
                        questionText.setText("Верно. Махмут Аипов дошёл до Берлина в 1945 году.");
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        Button myButton = new Button(this);
                        myButton.setText("Далее");
                        myButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                stage++;
                                setStage();
                            }
                        });

                        ll.addView(myButton, lp);
                        ll.requestLayout();
                        break;
                    }
                    case 2: {
                        Button myButton;
                        String question = "Кто выше по званию: старший лейтенант или старший сержант?";
                        String[] answers = {"Старший лейтенант", "Старший сержант"};
                        int right = 1;
                        right--;

                        TextView questionText = new TextView(this);
                        questionText.setText(question);
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        for (int i = 0; i < answers.length; i++) {
                            if (i == right) {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stage++;
                                        setStage();
                                    }
                                });
                                ll.addView(myButton, lp);
                            } else {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        v.setBackgroundColor(0xFFFFB6C1);
                                    }
                                });
                                ll.addView(myButton, lp);
                            }
                        }

                        ll.requestLayout();
                        break;
                    }
                    case 3: {
                        TextView questionText = new TextView(this);
                        questionText.setText("Верно. Рота входит в состав батальона. В батальон может входить 3-4 полка.");
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        Button myButton = new Button(this);
                        myButton.setText("Далее");
                        myButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                stage++;
                                setStage();
                            }
                        });

                        ll.addView(myButton, lp);
                        ll.requestLayout();
                        break;
                    }
                    case 4: {
                        Button myButton;
                        String question = "Плацдарм – участок местности, которым овладевали наступающие войска при форсировании водной преграды или удерживаемый отступающими войсками на ее противоположном берегу. А что значит «форсировать водную преграду»? ";
                        String[] answers = {"Устранить водную преграду", "Преодолеть водную преграду", "Создать водную преграду"};
                        int right = 2;
                        right--;

                        TextView questionText = new TextView(this);
                        questionText.setText(question);
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        for (int i = 0; i < answers.length; i++) {
                            if (i == right) {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stage++;
                                        setStage();
                                    }
                                });
                                ll.addView(myButton, lp);
                            } else {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        v.setBackgroundColor(0xFFFFB6C1);
                                    }
                                });
                                ll.addView(myButton, lp);
                            }
                        }

                        ll.requestLayout();
                        break;
                    }
                    case 5: {
                        TextView questionText = new TextView(this);
                        questionText.setText("Верно. От Костычей до Берлина идти 540 часов. Это 22,5 дня без перерыва.");
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        Button myButton = new Button(this);
                        myButton.setText("Далее");
                        myButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SharedPreferences sharedPref = mainActivity.getPreferences(Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.putBoolean("4", true);
                                editor.commit();
                                mainActivity.canRead = true;
                                mainActivity.refreshPhoto();
                                finish();
                            }
                        });

                        ll.addView(myButton, lp);
                        ll.requestLayout();
                        break;
                    }
                }
                break;
            case 5:
                ((ImageView) findViewById(R.id.imageView)).setImageResource(R.drawable.question_5);
                switch (stage) {
                    case 0: {
                        Button myButton;
                        String question = "Кто такой командир?";
                        String[] answers = {"Лицо командного и начальствующего состава в вооружённых силах и иных силовых структурах", "Низшее, первое воинское звание в Вооружённых Силах", "Чин, воинское звание солдата"};
                        int right = 2;
                        right--;

                        TextView questionText = new TextView(this);
                        questionText.setText(question);
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        for (int i = 0; i < answers.length; i++) {
                            if (i == right) {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stage++;
                                        setStage();
                                    }
                                });
                                ll.addView(myButton, lp);
                            } else {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        v.setBackgroundColor(0xFFFFB6C1);
                                    }
                                });
                                ll.addView(myButton, lp);
                            }
                        }

                        ll.requestLayout();
                        break;
                    }
                    case 1: {
                        TextView questionText = new TextView(this);
                        questionText.setText("Верно. Аипов был ранен 4 раза. Два последних раза он был ранен в смертельном героическом бою, после которого погиб от ран.");
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        Button myButton = new Button(this);
                        myButton.setText("Далее");
                        myButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                stage++;
                                setStage();
                            }
                        });

                        ll.addView(myButton, lp);
                        ll.requestLayout();
                        break;
                    }
                    case 2: {
                        Button myButton;
                        String question = "Сколько единиц советской бронетехники было потеряно в 1943 году?";
                        String[] answers = {"3377", "12079", "8995", "1488"};
                        int right = 3;
                        right--;

                        TextView questionText = new TextView(this);
                        questionText.setText(question);
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        for (int i = 0; i < answers.length; i++) {
                            if (i == right) {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stage++;
                                        setStage();
                                    }
                                });
                                ll.addView(myButton, lp);
                            } else {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        v.setBackgroundColor(0xFFFFB6C1);
                                    }
                                });
                                ll.addView(myButton, lp);
                            }
                        }

                        ll.requestLayout();
                        break;
                    }
                    case 3: {
                        TextView questionText = new TextView(this);
                        questionText.setText("Верно. Победа и мир наступили через 17 дней после того, как героически погиб Махмут Ильячевич.");
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        Button myButton = new Button(this);
                        myButton.setText("Далее");
                        myButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                stage++;
                                setStage();
                            }
                        });

                        ll.addView(myButton, lp);
                        ll.requestLayout();
                        break;
                    }
                    case 4: {
                        Button myButton;
                        String question = "Что такое верхмат?";
                        String[] answers = {"Вооружённые силы нацистской Германии в 1935-1945 годах", "Вооруженные силы СССР в 1941-1945 годах"};
                        int right = 1;
                        right--;

                        TextView questionText = new TextView(this);
                        questionText.setText(question);
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        for (int i = 0; i < answers.length; i++) {
                            if (i == right) {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stage++;
                                        setStage();
                                    }
                                });
                                ll.addView(myButton, lp);
                            } else {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        v.setBackgroundColor(0xFFFFB6C1);
                                    }
                                });
                                ll.addView(myButton, lp);
                            }
                        }

                        ll.requestLayout();
                        break;
                    }
                    case 5: {
                        TextView questionText = new TextView(this);
                        questionText.setText("Верно. Победа и мир наступили через 17 дней после того, как героически погиб Махмут Ильячевич.");
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        Button myButton = new Button(this);
                        myButton.setText("Далее");
                        myButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SharedPreferences sharedPref = mainActivity.getPreferences(Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.putBoolean("5", true);
                                editor.commit();
                                mainActivity.canRead = true;
                                mainActivity.refreshPhoto();
                                finish();
                            }
                        });

                        ll.addView(myButton, lp);
                        ll.requestLayout();
                        break;
                    }
                }
                break;
            case 6:
                ((ImageView) findViewById(R.id.imageView)).setImageResource(R.drawable.question_6);
                switch (stage){

                    case 0: {
                        Button myButton;
                        String question = "За какие подвиги присваивали звание Героя Советского Союза?";
                        String[] answers = {"За успешные результаты в ведении боевых действий", "За испытания новейших технологий в важной военной и гражданской технике, что было связано с большими рисками", "Все варианты"};
                        int right = 3;
                        right--;

                        TextView questionText = new TextView(this);
                        questionText.setText(question);
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        for (int i = 0; i < answers.length; i++) {
                            if (i == right) {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stage++;
                                        setStage();
                                    }
                                });
                                ll.addView(myButton, lp);
                            } else {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        v.setBackgroundColor(0xFFFFB6C1);
                                    }
                                });
                                ll.addView(myButton, lp);
                            }
                        }

                        ll.requestLayout();
                        break;
                    }
                    case 1: {
                        TextView questionText = new TextView(this);
                        questionText.setText("Верно. Махмут Ильячевич, кроме Героя Советского Союза, посмертно получил Орден Ленина.");
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        Button myButton = new Button(this);
                        myButton.setText("Далее");
                        myButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                stage++;
                                setStage();
                            }
                        });

                        ll.addView(myButton, lp);
                        ll.requestLayout();
                        break;
                    }
                    case 2: {
                        Button myButton;
                        String question = "За время Великой Отечественной войны звание Героя Советского Союза получили официально 11657 человек, среди них есть и женщины. А сколько женщин было удостоено этого звания?";
                        String[] answers = {"89", "93", "90", "87"};
                        int right = 3;
                        right--;

                        TextView questionText = new TextView(this);
                        questionText.setText(question);
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        for (int i = 0; i < answers.length; i++) {
                            if (i == right) {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stage++;
                                        setStage();
                                    }
                                });
                                ll.addView(myButton, lp);
                            } else {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        v.setBackgroundColor(0xFFFFB6C1);
                                    }
                                });
                                ll.addView(myButton, lp);
                            }
                        }

                        ll.requestLayout();
                        break;
                    }
                    case 3: {
                        TextView questionText = new TextView(this);
                        questionText.setText("Верно. Школе было 66 лет.");
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        Button myButton = new Button(this);
                        myButton.setText("Далее");
                        myButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                stage++;
                                setStage();
                            }
                        });

                        ll.addView(myButton, lp);
                        ll.requestLayout();
                        break;
                    }
                    case 4: {
                        Button myButton;
                        String question = "Кто удостаивался звания Героя Советского Союза трижды?";
                        String[] answers = {"Будённый Семён Михайлович", "Кожедуб Иван Никитович", "Покрышкин Александр Иванович", "Все варианты"};
                        int right = 4;
                        right--;

                        TextView questionText = new TextView(this);
                        questionText.setText(question);
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        for (int i = 0; i < answers.length; i++) {
                            if (i == right) {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stage++;
                                        setStage();
                                    }
                                });
                                ll.addView(myButton, lp);
                            } else {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        v.setBackgroundColor(0xFFFFB6C1);
                                    }
                                });
                                ll.addView(myButton, lp);
                            }
                        }

                        ll.requestLayout();
                        break;
                    }
                    case 5: {
                        TextView questionText = new TextView(this);
                        questionText.setText("Верно. Школе было 66 лет.");
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        Button myButton = new Button(this);
                        myButton.setText("Далее");
                        myButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                stage++;
                                setStage();
                            }
                        });

                        ll.addView(myButton, lp);
                        ll.requestLayout();
                        break;
                    }
                    case 6: {
                        Button myButton;
                        String question = "Что такое тактика?";
                        String[] answers = {"Составная часть военного искусства, включающая теорию и практику подготовки и ведения боя", "Наука о ведении войны, одна из областей военного искусства, высшее его проявление", "Все варианты"};
                        int right = 1;
                        right--;

                        TextView questionText = new TextView(this);
                        questionText.setText(question);
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        for (int i = 0; i < answers.length; i++) {
                            if (i == right) {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stage++;
                                        setStage();
                                    }
                                });
                                ll.addView(myButton, lp);
                            } else {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        v.setBackgroundColor(0xFFFFB6C1);
                                    }
                                });
                                ll.addView(myButton, lp);
                            }
                        }

                        ll.requestLayout();
                        break;
                    }
                    case 7: {
                        TextView questionText = new TextView(this);
                        questionText.setText("Верно. Школе было 66 лет.");
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        Button myButton = new Button(this);
                        myButton.setText("Далее");
                        myButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                stage++;
                                setStage();
                            }
                        });

                        ll.addView(myButton, lp);
                        ll.requestLayout();
                        break;
                    }
                    case 8: {
                        Button myButton;
                        String question = "Стрелять «прямой наводкой». Что обозначает данный приказ командира артиллерийскому расчету?";
                        String[] answers = {"Стрельба с закрытых позиций, ведение артиллерийского огня по целям, которые не находятся в прямой видимости с огневой позиции", "Стрельба с открытой огневой позиции, когда орудие наводится непосредственно в цель", "Стрельба артиллерии при углах возвышения до 20°", "Все варианты"};
                        int right = 2;
                        right--;

                        TextView questionText = new TextView(this);
                        questionText.setText(question);
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        for (int i = 0; i < answers.length; i++) {
                            if (i == right) {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stage++;
                                        setStage();
                                    }
                                });
                                ll.addView(myButton, lp);
                            } else {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        v.setBackgroundColor(0xFFFFB6C1);
                                    }
                                });
                                ll.addView(myButton, lp);
                            }
                        }

                        ll.requestLayout();
                        break;
                    }
                    case 9: {
                        TextView questionText = new TextView(this);
                        questionText.setText("Верно. Школе было 66 лет.");
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        Button myButton = new Button(this);
                        myButton.setText("Далее");
                        myButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                stage++;
                                setStage();
                            }
                        });

                        ll.addView(myButton, lp);
                        ll.requestLayout();
                        break;
                    }
                    case 10: {
                        Button myButton;
                        String question = "Стрелять «Беглым!». Что обозначает данный приказ командира артиллерийскому расчету?";
                        String[] answers = {"Стрельба с одной огневой позиции в обороне, при действиях из засад", "Стрельба с закрытых позиций, ведение артиллерийского огня по целям, которые не находятся в прямой видимости с огневой позиции", "Стрельба не по команде (как при стрельбе залпами), а немедленно по готовности", "Все варианты"};
                        int right = 3;
                        right--;

                        TextView questionText = new TextView(this);
                        questionText.setText(question);
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        for (int i = 0; i < answers.length; i++) {
                            if (i == right) {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        stage++;
                                        setStage();
                                    }
                                });
                                ll.addView(myButton, lp);
                            } else {
                                myButton = new Button(this);
                                myButton.setText(answers[i]);
                                myButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        v.setBackgroundColor(0xFFFFB6C1);
                                    }
                                });
                                ll.addView(myButton, lp);
                            }
                        }

                        ll.requestLayout();
                        break;
                    }
                    case 11: {
                        TextView questionText = new TextView(this);
                        questionText.setText("Верно. Школе было 66 лет.");
                        questionText.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
                        questionText.setTextSize(24);
                        questionText.setTextColor(Color.BLACK);
                        ll.addView(questionText, lp);

                        Button myButton = new Button(this);
                        myButton.setText("Далее");
                        myButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                SharedPreferences sharedPref = mainActivity.getPreferences(Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.putBoolean("6", true);
                                editor.commit();
                                mainActivity.canRead = true;
                                mainActivity.refreshPhoto();
                                finish();
                            }
                        });

                        ll.addView(myButton, lp);
                        ll.requestLayout();
                        break;
                    }
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        mainActivity.canRead = true;
        super.onBackPressed();
    }
}
