package com.example.qrgames;

import android.Manifest;
import android.app.ActionBar;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.net.Uri;
import android.os.Build;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.qrgames.camera.CameraSource;
import com.example.qrgames.camera.CameraSourcePreview;
import com.example.qrgames.others.GraphicOverlay;
import com.example.qrgames.others.QrCallback;
import com.example.qrgames.qr_detection.QrRecognitionProcessor;
import com.google.firebase.FirebaseApp;

import java.io.File;
import java.io.IOException;

//https://vk.com/aaakkkaaa

public class MainActivity extends AppCompatActivity implements QrCallback.Callback {
    private CameraSource cameraSource = null;
    private CameraSourcePreview preview;
    private GraphicOverlay graphicOverlay;
    private QrCallback qrCallback;
    public boolean canRead = false;

    private TextView hintCamera;
    private String focusedQR = "";
    boolean p1, p2, p3, p4, p5, p6;


    private static String TAG = MainActivity.class.getSimpleName().toString().trim();

    public void refreshPhoto() {
        final SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        p1 = sharedPref.getBoolean("1", false);
        p2 = sharedPref.getBoolean("2", false);
        p3 = sharedPref.getBoolean("3", false);
        p4 = sharedPref.getBoolean("4", false);
        p5 = sharedPref.getBoolean("5", false);
        p6 = sharedPref.getBoolean("6", false);

        if (p1) {
            ((ImageView) findViewById(R.id.imageView3)).setImageResource(R.drawable.not_focused_1_opened);
        } else {
            ((ImageView) findViewById(R.id.imageView3)).setImageResource(R.drawable.not_focused_1_closed);
        }
        if (p2) {
            ((ImageView) findViewById(R.id.imageView6)).setImageResource(R.drawable.not_focused_2_opened);
        } else {
            ((ImageView) findViewById(R.id.imageView6)).setImageResource(R.drawable.not_focused_2_closed);
        }
        if (p3) {
            ((ImageView) findViewById(R.id.imageView4)).setImageResource(R.drawable.not_focused_3_opened);
        } else {
            ((ImageView) findViewById(R.id.imageView4)).setImageResource(R.drawable.not_focused_3_closed);
        }
        if (p4) {
            ((ImageView) findViewById(R.id.imageView7)).setImageResource(R.drawable.not_focused_4_opened);
        } else {
            ((ImageView) findViewById(R.id.imageView7)).setImageResource(R.drawable.not_focused_4_closed);
        }
        if (p5) {
            ((ImageView) findViewById(R.id.imageView5)).setImageResource(R.drawable.not_focused_5_opened);
        } else {
            ((ImageView) findViewById(R.id.imageView5)).setImageResource(R.drawable.not_focused_5_closed);
        }
        if (p6) {
            ((ImageView) findViewById(R.id.imageView8)).setImageResource(R.drawable.not_focused_6_opened);
        } else {
            ((ImageView) findViewById(R.id.imageView8)).setImageResource(R.drawable.not_focused_6_closed);
        }

        if (p1 && p2 && p3 && p4 && p5 && p6) {
            (findViewById(R.id.imageViewStar)).setAlpha(1.0f);
            (findViewById(R.id.imageViewStar)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_DENIED || ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_DENIED) {
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 191);

                    } else {
                        shareFinish();
                    }
                }
            });
        } else {
            (findViewById(R.id.imageViewStar)).setAlpha(0.7f);
            (findViewById(R.id.imageViewStar)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
        }
    }

    void shareFinish() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        final EditText edittext = new EditText(this);
        alert.setMessage("Введите ваши имя и фамилию в родительном падеже.");
        alert.setTitle("Получить диплом");

        alert.setView(edittext);

        alert.setPositiveButton("Поделиться", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                shareToSocials(edittext.getText().toString());
            }
        });

        alert.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // what ever you want to do with No option.
            }
        });

        alert.show();
    }

    void shareToSocials(String name) {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Bitmap diplom = BitmapFactory.decodeResource(getResources(),
                R.drawable.diplom);

        android.graphics.Bitmap.Config bitmapConfig = diplom.getConfig();
        if(bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }

        diplom = diplom.copy(bitmapConfig, true);

        Canvas canvas = new Canvas(diplom);

        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(170);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);
        paint.setTypeface(ResourcesCompat.getFont(this, R.font.font_custom));
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));

        canvas.drawText("Диплом выдан", diplom.getWidth()/2, diplom.getHeight()/2, paint);

        paint.setTextSize(200);
        canvas.drawText(name, diplom.getWidth()/2, diplom.getHeight()/2+220, paint);

        paint.setTextSize(75);
        canvas.drawText("©microcodica | vk.com/micropolis.center", diplom.getWidth()/2, diplom.getHeight()-200, paint);

        String pathofBmp = MediaStore.Images.Media.insertImage(getContentResolver(), diplom, "title", null);
        Uri bmpUri = Uri.parse(pathofBmp);

        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("image/*");

        shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Я прошёл игру \"Код Заруднева\"!\nСоветую и вам сыграть в эту увлекательную игру!\nhttps://vk.com/micropolis.center");
        startActivity(Intent.createChooser(shareIntent, "Share Image"));
    }

    @Override
    public void onBackPressed() {
        ((ScrollView) findViewById(R.id.scrollView2)).fullScroll(((ScrollView) findViewById(R.id.scrollView2)).FOCUS_UP);
        canRead = false;
    }

    @Override
    public void callingBack(String s) {
        synchronized (qrCallback) {
            if (s != null && focusedQR.equals(s) && canRead) {
                canRead = false;
                switch (s) {
                    case "1 слайд":
                        QuestionsActivity.qrText = 1;
                        QuestionsActivity.mainActivity = this;

                        Intent myIntent = new Intent(MainActivity.this, QuestionsActivity.class);
                        MainActivity.this.startActivity(myIntent);
                        break;
                    case "2 слайд":
                        QuestionsActivity.qrText = 2;
                        QuestionsActivity.mainActivity = this;

                        Intent myIntent1 = new Intent(MainActivity.this, QuestionsActivity.class);
                        MainActivity.this.startActivity(myIntent1);
                        break;
                    case "3 слайд":
                        QuestionsActivity.qrText = 3;
                        QuestionsActivity.mainActivity = this;

                        Intent myIntent2 = new Intent(MainActivity.this, QuestionsActivity.class);
                        MainActivity.this.startActivity(myIntent2);
                        break;
                    case "4 слайд":
                        QuestionsActivity.qrText = 4;
                        QuestionsActivity.mainActivity = this;

                        Intent myIntent3 = new Intent(MainActivity.this, QuestionsActivity.class);
                        MainActivity.this.startActivity(myIntent3);
                        break;
                    case "5 слайд":
                        QuestionsActivity.qrText = 5;
                        QuestionsActivity.mainActivity = this;

                        Intent myIntent4 = new Intent(MainActivity.this, QuestionsActivity.class);
                        MainActivity.this.startActivity(myIntent4);
                        break;
                    case "6 слайд":
                        QuestionsActivity.qrText = 6;
                        QuestionsActivity.mainActivity = this;

                        Intent myIntent5 = new Intent(MainActivity.this, QuestionsActivity.class);
                        MainActivity.this.startActivity(myIntent5);
                        break;
                    default:
                        canRead = true;
                        break;
                }
                Log.e("MLKIT", s);
            }
        }
    }

    @Override
    public void callingBackCanRead(boolean s) {
        canRead = s;
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context) {
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px      A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context) {
        return px / ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    private void setCameraHint(String hint) {
        hintCamera.setText(hint);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        getSupportActionBar().hide();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        getWindow().setNavigationBarColor(Color.BLACK);

        setContentView(R.layout.activity_main);

        hintCamera = findViewById(R.id.textView3);

        refreshPhoto();

        findViewById(R.id.scrollView2).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        findViewById(R.id.imageView3).setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ((ScrollView) findViewById(R.id.scrollView2)).fullScroll(((ScrollView) findViewById(R.id.scrollView2)).FOCUS_DOWN);
                canRead = true;
                setCameraHint("Наведите камеру на слайд 1");
                focusedQR = "1 слайд";
                refreshPhoto();
                if (p1) {
                    ((ImageView) findViewById(R.id.imageView3)).setImageResource(R.drawable.focused_1_opened);
                } else {
                    ((ImageView) findViewById(R.id.imageView3)).setImageResource(R.drawable.focused_1_closed);
                }

                callingBack(focusedQR);
                return false;
            }
        });

        findViewById(R.id.imageView4).setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ((ScrollView) findViewById(R.id.scrollView2)).fullScroll(((ScrollView) findViewById(R.id.scrollView2)).FOCUS_DOWN);
                canRead = true;
                setCameraHint("Наведите камеру на слайд 3");
                focusedQR = "3 слайд";
                refreshPhoto();
                if (p3) {
                    ((ImageView) findViewById(R.id.imageView4)).setImageResource(R.drawable.focused_3_opened);
                } else {
                    ((ImageView) findViewById(R.id.imageView4)).setImageResource(R.drawable.focused_3_closed);
                }

                callingBack(focusedQR);
                return false;
            }
        });

        findViewById(R.id.imageView5).setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ((ScrollView) findViewById(R.id.scrollView2)).fullScroll(((ScrollView) findViewById(R.id.scrollView2)).FOCUS_DOWN);
                canRead = true;
                setCameraHint("Наведите камеру на слайд 5");
                focusedQR = "5 слайд";
                refreshPhoto();
                if (p5) {
                    ((ImageView) findViewById(R.id.imageView5)).setImageResource(R.drawable.focused_5_opened);
                } else {
                    ((ImageView) findViewById(R.id.imageView5)).setImageResource(R.drawable.focused_5_closed);
                }

                callingBack(focusedQR);
                return false;
            }
        });

        findViewById(R.id.imageView6).setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ((ScrollView) findViewById(R.id.scrollView2)).fullScroll(((ScrollView) findViewById(R.id.scrollView2)).FOCUS_DOWN);
                canRead = true;
                setCameraHint("Наведите камеру на слайд 2");
                focusedQR = "2 слайд";
                refreshPhoto();
                if (p2) {
                    ((ImageView) findViewById(R.id.imageView6)).setImageResource(R.drawable.focused_2_opened);
                } else {
                    ((ImageView) findViewById(R.id.imageView6)).setImageResource(R.drawable.focused_2_closed);
                }

                callingBack(focusedQR);
                return false;
            }
        });

        findViewById(R.id.imageView7).setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ((ScrollView) findViewById(R.id.scrollView2)).fullScroll(((ScrollView) findViewById(R.id.scrollView2)).FOCUS_DOWN);
                canRead = true;
                setCameraHint("Наведите камеру на слайд 4");
                focusedQR = "4 слайд";
                refreshPhoto();
                if (p4) {
                    ((ImageView) findViewById(R.id.imageView7)).setImageResource(R.drawable.focused_4_opened);
                } else {
                    ((ImageView) findViewById(R.id.imageView7)).setImageResource(R.drawable.focused_4_closed);
                }

                callingBack(focusedQR);
                return false;
            }
        });

        findViewById(R.id.imageView8).setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ((ScrollView) findViewById(R.id.scrollView2)).fullScroll(((ScrollView) findViewById(R.id.scrollView2)).FOCUS_DOWN);
                canRead = true;
                setCameraHint("Наведите камеру на слайд 6");
                focusedQR = "6 слайд";
                refreshPhoto();
                if (p6) {
                    ((ImageView) findViewById(R.id.imageView8)).setImageResource(R.drawable.focused_6_opened);
                } else {
                    ((ImageView) findViewById(R.id.imageView8)).setImageResource(R.drawable.focused_6_closed);
                }

                callingBack(focusedQR);
                return false;
            }
        });

        findViewById(R.id.imageView9).setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ((ScrollView) findViewById(R.id.scrollView2)).fullScroll(((ScrollView) findViewById(R.id.scrollView2)).FOCUS_UP);
                canRead = false;
                focusedQR = "";
                refreshPhoto();
                return false;
            }
        });

        final ConstraintLayout layout = (ConstraintLayout) findViewById(R.id.linearContent);
        layout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                layout.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int ScreenHeight = size.y;

                layout.getLayoutParams().height = ScreenHeight;
                layout.requestLayout();

            }
        });

        qrCallback = new QrCallback();
        qrCallback.registerCallBack(this);


        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, 190);

        } else {
            runCamera();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 190) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                runCamera();
            } else {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, 190);
            }
        }
        if (requestCode == 191) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                shareFinish();
            }
        }
    }

    void runCamera() {
        preview = (CameraSourcePreview) findViewById(R.id.camera_source_preview);
        if (preview == null) {
            Log.d(TAG, "Preview is null");
        }
        graphicOverlay = (GraphicOverlay) findViewById(R.id.graphics_overlay);
        if (graphicOverlay == null) {
            Log.d(TAG, "graphicOverlay is null");
        }

        createCameraSource();
        startCameraSource();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        if (cameraSource != null) {
            startCameraSource();
        }
    }

    /**
     * Stops the camera.
     */
    @Override
    protected void onPause() {
        super.onPause();
        if (cameraSource != null) {
            preview.stop();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (cameraSource != null) {
            cameraSource.release();
        }
    }

    private void createCameraSource() {

        if (cameraSource == null) {
            cameraSource = new CameraSource(this, graphicOverlay, qrCallback);
            cameraSource.setFacing(CameraSource.CAMERA_FACING_BACK);
        }

        cameraSource.setMachineLearningFrameProcessor(new QrRecognitionProcessor());
    }

    private void startCameraSource() {
        if (cameraSource != null) {
            try {
                if (preview == null) {
                    Log.d(TAG, "resume: Preview is null");
                }
                if (graphicOverlay == null) {
                    Log.d(TAG, "resume: graphOverlay is null");
                }
                preview.start(cameraSource, graphicOverlay);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                cameraSource.release();
                cameraSource = null;
            }
        }
    }
}
